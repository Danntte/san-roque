<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/createHomeSilder', 'HomeController@createHomeSilder')->name('createHomeSilder');

Route::get('/', 'PageController@welcome')->name('welcome');

Route::get('/adogta', 'PageController@adogta')->name('adogta');

Route::get('/help', 'PageController@help')->name('help');

Route::get('/theteam', 'PageController@theteam')->name('theteam');

Route::get('/activities', 'PageController@activities')->name('activities');

Route::get('/contactUs', 'PageController@contactUs')->name('contactUs');
