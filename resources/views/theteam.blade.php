@section('name')
    El Equipo
@endsection
@extends('layouts.app')

@section('content')
<div class="streak streak-md streak-photo" style="background-image:url('https://mdbootstrap.com/img/Photos/Horizontal/Nature/12-col/img(115).jpg')">
    <div class="flex-center white-text rgba-black-light">
      <ul class="mb-0 list-unstyled">
        <li>
          <h2 class="h2-responsive"> NUESTROS COLABORADORES, SIN ELLOS NO SERIA POSIBLE.</h2>
        </li>
        <li class="mb-0">
          <h5 class="text-center font-italic mb-0">¡Gracias!</h5>
        </li>
      </ul>
     </div>
</div>
<div class="container">
    <hr>
    <div class="row">
        @php
            $row_count;
            $row_count = 0;
        @endphp
        @foreach ($members as $member)
        <div class="col-4">
            <!-- Card -->
            <div class="card testimonial-card">

                <!-- Background color -->
                <div class="card-up indigo lighten-1"></div>

                <!-- Avatar -->
                <div class="avatar mx-auto white">
                <img src="https://mdbootstrap.com/img/Photos/Avatars/img%20%2810%29.jpg" class="rounded-circle" alt="woman avatar">
                </div>
                <!-- Content -->
                <div class="card-body">
                <!-- Name -->
                <h4 class="card-title">{{$member->name}} {{$member->last_name}}</h4>
                <hr>
                <!-- Quotation -->
                <p>{{$member->description}}</p>
                </div>

            </div>
            <!-- Card -->
        </div>
        @if ($row_count == 2)
        @php
            $row_count = 0;
        @endphp
        </div>
        <br>
        <div class="row">
        @endif
        @php
            $row_count = $row_count + 1;
        @endphp
        @endforeach
    </div>
    <hr>
</div>
@endsection

