@section('name')
    Adogta
@endsection
@extends('layouts.app')

@section('content')
<div class="streak streak-md streak-photo" style="background-image:url('https://mdbootstrap.com/img/Photos/Horizontal/Nature/12-col/img(115).jpg')">
    <div class="flex-center white-text rgba-black-light">
      <ul class="mb-0 list-unstyled">
        <li>
          <h2 class="h2-responsive"><i class="fas fa-quote-left" aria-hidden="true"></i> Para cambiar la vida de un perrito solo hace falta voluntad. <i class="fas fa-quote-right"
              aria-hidden="true"></i></h2>
        </li>
        <li class="mb-0">
          <h5 class="text-center font-italic mb-0">~ Steve Jobs</h5>
        </li>
      </ul>
     </div>
</div>
<hr>
<section class="container">
    <div class="row">
        @php
            $row_count;
            $row_count = 0;
        @endphp
        @foreach ($dogs as $dog)
        <div class="col-4">
            <div class="card">
                <!-- Card image -->
                <div class="view overlay">
                  <img class="card-img-top" src="https://mdbootstrap.com/img/Mockups/Lightbox/Thumbnail/img%20(67).jpg"
                    alt="Card image cap">
                  <a href="#!">
                    <div class="mask rgba-white-slight"></div>
                  </a>
                </div>
                <!-- Card content -->
                <div class="card-body text-center">
                  <!-- Title -->
                  <h4 class="card-title">{{$dog->name_dog}}</h4>
                  <!-- Text -->
                  <p class="card-text">{{$dog->description}}</p>
                  <!-- Button -->
                  <a href="#" class="btn btn-sm btn-deep-purple btn-rounded"><i class="fas fa-baby-carriage"></i> Adoptalo</a>
                  <a href="#" class="btn btn-sm btn-dark-green btn-rounded"><i class="fas fa-heart"></i> Apadrinalo</a>
                </div>
              </div>
        </div>
        @if ($row_count == 2)
        @php
            $row_count = 0;
        @endphp
        </div>
        <br>
        <div class="row">
        @endif
        @php
            $row_count = $row_count + 1;
        @endphp
        @endforeach
    </div>
</div>
</section>
<hr>
@endsection
