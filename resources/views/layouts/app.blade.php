<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{config('app.name', 'Laravel')}} - @yield('name')</title>

          <!-- Required meta tags always come first -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
        <!-- Bootstrap core CSS -->
        <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
        <!-- Material Design Bootstrap -->
        <link href="{{asset('css/mdb.min.css')}}" rel="stylesheet">
        <!-- Additional CSS Styles -->
        <link href="{{asset('css/style.css')}}" rel="stylesheet">
    </head>
    <body @isset($backgroundColor) class="unique-color-dark" @endisset>
        <!--Main Navigation-->
        <header>
            <nav class="navbar navbar-expand-lg navbar-dark @if(empty($adminViews)) fixed-top @endif scrolling-navbar">
            <div class="container">
                <a class="navbar-brand" href="{{route('welcome')}}">
                    <img src="{{asset('img/logo-san-roque.png')}}" height="50" alt="mdb logo">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-7"
                aria-controls="navbarSupportedContent-7" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent-7">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item @isset($welcome) active @endisset">
                    <a class="nav-link" href="{{route('welcome')}}">Sobre Nosostros <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item @isset($adogta) active @endisset">
                    <a class="nav-link" href="{{route('adogta')}}">Adogta</a>
                    </li>
                    <li class="nav-item @isset($theteam) active @endisset">
                    <a class="nav-link" href="{{route('theteam')}}">El Equipo</a>
                    </li>
                    <li class="nav-item @isset($help) active @endisset">
                        <a class="nav-link" href="{{route('help')}}">¿Como nos puedes ayudar?</a>
                    </li>
                    <li class="nav-item @isset($activities) active @endisset">
                        <a class="nav-link" href="{{route('activities')}}">Actividades</a>
                    </li>
                    <li class="nav-item @isset($contactUs) active @endisset">
                        <a class="nav-link" href="{{route('contactUs')}}">Contactanos</a>
                    </li>
                </ul>
                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                        @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                        @endif
                    @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                @if (Auth::user()->role == 'admin')
                                <a class="dropdown-item" href="{{ route('home') }}"> Panel de Administracion</a>
                                @endif
                                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
                </div>
            </div>
            </nav>

        </header>
        @yield('content')
        @component('layouts.footer')

        @endcomponent

        <!--  SCRIPTS  -->
        <!-- JQuery -->
        <script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>
        <!-- Bootstrap tooltips -->
        <script type="text/javascript" src="{{asset('js/popper.min.js')}}"></script>
        <!-- Bootstrap core JavaScript -->
        <script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
        <!-- MDB core JavaScript -->
        <script type="text/javascript" src="{{asset('js/mdb.min.js')}}"></script>
    </body>
</html>
