@section('name')
    Verificacion
@endsection
@php
    $backgroundColor = true
@endphp
@extends('layouts.app')

@section('content')
<div class="container h-100 align-content-center">
    <div class="row justify-content-center vertical-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header blue darken-4 text-white">{{ __('Verify Your Email Address') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                        </div>
                    @endif

                    {{ __('Before proceeding, please check your email for a verification link.') }}
                    {{ __('If you did not receive the email') }}, <a href="{{ route('verification.resend') }}">{{ __('click here to request another') }}</a>.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
