@section('name')
    Ayudanos
@endsection
@extends('layouts.app')

@section('content')
<div class="streak streak-md streak-photo" style="background-image:url('https://mdbootstrap.com/img/Photos/Horizontal/Nature/12-col/img(115).jpg')">
    <div class="flex-center white-text rgba-black-light">
      <ul class="mb-0 list-unstyled">
        <li>
          <h2 class="h2-responsive"><i class="fas fa-quote-left" aria-hidden="true"></i> Para cambiar la vida de un perrito solo hace falta voluntad. <i class="fas fa-quote-right"
              aria-hidden="true"></i></h2>
        </li>
        <li class="mb-0">
          <h5 class="text-center font-italic mb-0">~ Steve Jobs</h5>
        </li>
      </ul>
     </div>
</div>
@endsection

