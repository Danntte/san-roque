@section('name')
    Home Admin
@endsection
@php
    $backgroundColor = true;
    $adminViews = true;
@endphp
@extends('layouts.app')
@section('content')
<section class="h-100">
    <div class="container">
        <hr class="white">
        <div class="row">
            <div class="col-12">
                <h2 class="text-center white-text">Panel de Administracion</h2>
            </div>
        </div>
        <hr class="white">
        <div class="row">
            <div class="col-3">
              <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-welcome" role="tab"
                  aria-controls="v-pills-home" aria-selected="true">Sobre Nosotros</a>
                <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-adgota" role="tab"
                  aria-controls="v-pills-profile" aria-selected="false">Adgota</a>
                <a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-team" role="tab"
                  aria-controls="v-pills-messages" aria-selected="false">El Equipo</a>
                <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-help" role="tab"
                  aria-controls="v-pills-settings" aria-selected="false">¿Como nos puedes ayudar?</a>
                  <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-activities" role="tab"
                  aria-controls="v-pills-profile" aria-selected="false">Actividades</a>
                  <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-contactUs" role="tab"
                  aria-controls="v-pills-profile" aria-selected="false">Contactanos</a>
                  <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-Users" role="tab"
                  aria-controls="v-pills-profile" aria-selected="false">Usuarios</a>
              </div>
            </div>
            <div class="col-9">
              <div class="tab-content" id="v-pills-tabContent">
                <div class="tab-pane fade show active" id="v-pills-welcome" role="tabpanel" aria-labelledby="v-pills-welcome-tab">
                    <h3 class="white-text">Subir Home Sliders</h3>
                    <form class="md-form" enctype="multipart/form-data" method="POST" action="{{action('HomeController@createHomeSilder')}}" accept-charset="UTF-8">
                        @csrf
                        <div class="row">
                            <div class="col-6">
                                <div class="md-form">
                                    <input type="text" id="message_big" class="form-control" name="message_big">
                                    <label for="message_big">Mensaje Principal</label>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="md-form">
                                    <input type="text" id="message_small" class="form-control" name="message_small">
                                    <label for="message_small">Mensaje Secundario</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="md-form">
                                    <input type="text" id="url_1" class="form-control" name="url_1">
                                    <label for="url_1">URL 1</label>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="md-form">
                                    <input type="text" id="url_2" class="form-control" name="url_2">
                                    <label for="url_2">URL 2</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="md-form">
                                    <input type="text" id="button_1" class="form-control" name="button_1">
                                    <label for="button_1">Mensaje Boton 1</label>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="md-form">
                                    <input type="text" id="button_2" class="form-control" name="button_2">
                                    <label for="button_2">Mensaje Boton 2</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="file-field">
                                    <div class="btn btn-primary btn-sm float-left">
                                        <span>Selecionar imagen</span>
                                        <input type="file" accept="image/*" id="image_path" name="image_path" class="form-control" required>
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text" placeholder="Sube la imagen">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row p-2">
                            <div class="col">
                                <input class="btn btn-dark-green btn-sm" type="submit" value="Aplicar">
                            </div>
                        </div>
                    </form>
                    <br>
                    <h3 class="white-text">Subir Home Streaks</h3>
                    <form class="md-form">
                        <div class="row">
                            <div class="col-6">
                                <div class="md-form">
                                    <input type="text" id="inputMDEx" class="form-control">
                                    <label for="inputMDEx">Mensaje Principal</label>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="md-form">
                                    <input type="text" id="inputMDEx" class="form-control">
                                    <label for="inputMDEx">Mensaje Secundario</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="file-field">
                                    <div class="btn btn-primary btn-sm float-left">
                                        <span>Selecionar imagen</span>
                                        <input type="file" accept="image/*" id=''>
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text" placeholder="Sube la imagen">
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <!-- Material unchecked -->
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="materialUnchecked">
                                    <label class="form-check-label" for="materialUnchecked">Comillas</label>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="tab-pane fade" id="v-pills-adgota" role="tabpanel" aria-labelledby="v-pills-adgota-tab">

                </div>
                <div class="tab-pane fade" id="v-pills-team" role="tabpanel" aria-labelledby="v-pills-team-tab">

                </div>
                <div class="tab-pane fade" id="v-pills-help" role="tabpanel" aria-labelledby="v-pills-help-tab">

                </div>
                <div class="tab-pane fade" id="v-pills-activities" role="tabpanel" aria-labelledby="v-pills-activities-tab">

                </div>
                <div class="tab-pane fade" id="v-pills-contactUs" role="tabpanel" aria-labelledby="v-pills-contactUs-tab">

                </div>
                <div class="tab-pane fade" id="v-pills-Users" role="tabpanel" aria-labelledby="v-pills-Users-tab">

                </div>
              </div>
            </div>
          </div>
    </div>
</section>
@endsection
