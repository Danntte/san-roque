@extends('layouts.app')
@section('name')
    Welcome
@endsection
@section('content')
<!--Carousel Wrapper-->
<div id="carousel-main" class="carousel slide carousel-fade" data-ride="carousel">
  <!--Indicators-->
  <ol class="carousel-indicators">
    @foreach ($slider as $slide)
    <li data-target="#carousel-main" data-slide-to="{{$loop->iteration - 1}}" @if($loop->iteration == 1) class="active" @endif></li>
    @endforeach
  </ol>
  <!--/.Indicators-->
  <!--Slides-->
  <div class="carousel-inner" role="listbox">
    @if(count($slider) == 0)
    <div class="carousel-item active img-carousel-full" style="background-image: url({{asset('img/default-img.gif')}})">
        <div class="view">
            <div class="flex-center mask rgba-black-light white-text">
              <ul class="animated fadeInUp col-md-12 list-unstyled list-inline">
                <li>
                  <h1 class="text-dark font-weight-bold text-uppercase">Inserta Imagenes para el Slider</h1>
                </li>
                <li>
                  <p class="text-dark font-weight-bold text-uppercase py-4">Solo desde admin</p>
                </li>
              </ul>
            </div>
        </div>
    </div>
    @endif
    @foreach ($slider as $slide)
    <div class="carousel-item @if($loop->iteration == 1) active @endif img-carousel-full" style="background-image: url({{asset('img/'.$slide->image->src.'/'.$slide->image->name_img)}})">
        <div class="view">
            <div class="flex-center mask rgba-black-light white-text">
              <ul class="animated fadeInUp col-md-12 list-unstyled list-inline">
                <li>
                  <h1 class="font-weight-bold text-uppercase">{{$slide->message_big}}</h1>
                </li>
                <li>
                  <p class="font-weight-bold text-uppercase py-4">{{$slide->message_small}}</p>
                </li>
                @if ($slide->button_1 != '')
                <li class="list-inline-item">
                    <a target="_blank" href="{{$slide->url_1}}" class="btn btn-unique btn-lg btn-rounded mr-0">{{$slide->button_1}}</a>
                </li>
                @endif
                @if ($slide->button_2 != '')
                <li class="list-inline-item">
                <a target="_blank" href="{{$slide->url_2}}" class="btn btn-cyan btn-lg btn-rounded ml-0">{{$slide->button_2}}</a>
                </li>
                @endif
              </ul>
            </div>
        </div>
    </div>
    @endforeach
  </div>
  <!--/.Slides-->
  <!--Controls-->
  <a class="carousel-control-prev" href="#carousel-main" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carousel-main" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
  <!--/.Controls-->
</div>
<!--/.Carousel Wrapper-->

@foreach ($homeStreaks as $streak)
<div class="streak streak-lg streak-photo" style="background-image:url({{asset('img/968614313.jpg')}})">
    <div class="flex-center white-text rgba-black-light">
      <ul class="mb-0 list-unstyled">
        <li>
        <h2 class="h2-responsive">
            @if ($streak->quote == true)<i class="fas fa-quote-left" aria-hidden="true"></i>@endif
                {{$streak->message}}
            @if ($streak->quote == true)<i class="fas fa-quote-right"aria-hidden="true"></i>@endif
        </h2>
        </li>
        <li class="mb-0">
          <h5 class="text-center font-italic mb-0">{{$streak->text}}</h5>
        </li>
       </ul>
    </div>
</div>
@endforeach

@component('layouts.floatingButton')@endcomponent

@endsection
