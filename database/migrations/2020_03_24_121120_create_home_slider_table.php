<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomeSliderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('home_slider', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('message_big');
            $table->string('message_small')->nullable();
            $table->string('button_1')->nullable();
            $table->string('button_2')->nullable();
            $table->string('url_1')->nullable();
            $table->string('url_2')->nullable();
            $table->integer('id_image')->nullable();
            $table->boolean('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('home_slider');
    }
}
