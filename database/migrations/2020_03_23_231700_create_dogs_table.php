<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dogs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name_dog');
            $table->timestamp('dateBirth');
            $table->string('race');
            $table->string('availableFor');
            $table->integer('size');
            $table->boolean('vaccinated');
            $table->boolean('dewormed');
            $table->integer('weigth');
            $table->boolean('sterilized');
            $table->string('history');
            $table->string('description');
            $table->string('id_image');
            $table->string('id_user_adopted');
            $table->string('id_user_sponsor');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dogs');
    }
}
