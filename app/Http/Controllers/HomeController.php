<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\HomeSlider;
use App\Images;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('admin.home');
    }

    public function createHomeSilder(Request $request)
    {
        $image_path = $request->file('image_path');
        if($image_path){
            $image_path_time = time().$image_path->getClientOriginalName();

            Images::insert(['name_img'=> $image_path_time,
                            'src'=> 'otherImg',
                            'created_at'=>date('Y-m-d'),
                            'updated_at'=>date('Y-m-d')]);
            Storage::disk('otherImg')->put($image_path_time, File::get($image_path));
        }

        $id_image = Images::select('id')->where('name_img', '=', $image_path_time)->get();
        $message_big = $request->input('message_big');
        $message_small = $request->input('message_small');
        $button_1 = $request->input('button_1');
        $button_2 = $request->input('button_2');
        $url_1 = $request->input('url_1');
        $url_2 = $request->input('url_2');

        HomeSlider::insert(['message_big'=>$message_big,
                            'message_small'=>$message_small,
                            'button_1'=>$button_1,
                            'button_2'=>$button_2,
                            'url_1'=>$url_1,
                            'url_2'=>$url_2,
                            'id_image'=>$id_image[0]->id,
                            'status'=>true,
                            'created_at'=>date('Y-m-d'),
                            'updated_at'=>date('Y-m-d')]);

        return redirect('');
    }
}
