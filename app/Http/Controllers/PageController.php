<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HomeSlider;
use App\Streaks;
use App\Dogs;
use App\User;

class PageController extends Controller
{
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function welcome()
    {
        $slider = HomeSlider::where('status', '=', true)->limit(5)->get();
        $homeStreaks = Streaks::where('status', '=', true)->where('use_for', '=', 'home')->limit(5)->get();
        return view('welcome', ['welcome' => true,
                                'slider' => $slider,
                                'homeStreaks' => $homeStreaks]);
    }

    public function adogta()
    {
        $dogs = Dogs::where('availableFor', '=', 'adoption')->orWhere('availableFor', '=', 'sponsor')->orWhere('availableFor', '=', 'both')->get();
        return view('adogta', ['adogta' => true,
                               'dogs' => $dogs]);
    }

    public function help()
    {
        return view('help', ['help' => true]);
    }

    public function theteam()
    {
        $members = User::where('role', '=', 'team')->get();
        return view('theteam', ['theteam' => true,
                                'members' => $members]);
    }

    public function activities()
    {
        return view('activities', ['activities' => true]);
    }

    public function contactUs()
    {
        return view('contactUs', ['contactUs' => true]);
    }
}
