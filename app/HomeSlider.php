<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomeSlider extends Model
{
    protected $table = 'home_slider';

    public function image()
    {
        return $this->hasOne('App\Images', 'id', 'id_image');
    }
}
